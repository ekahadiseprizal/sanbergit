<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class BlogController extends Controller
{
    //

    // public function index($username)
    public function index()
    {
        $token = env('GITLAB_TOKEN');
        $username = env('USER_NAME');
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/ekahadiseprizal/projects";
        $header = array('headers' => array('access_token' =>  $token));
        $response = $client->get($uri, $header);
        $projectLists = json_decode($response->getBody()->getContents());
        return view('projects.index', compact('projectLists'));
    }
    public function getUserProject($username)
    {
        $token = env('GITLAB_TOKEN');
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/users/" . $username . "/projects";
        $header = array('headers' => array('access_token' =>  $token));
        $response = $client->get($uri, $header);
        $projectLists = json_decode($response->getBody()->getContents());
        return view('projects.index', compact('projectLists'));
    }

    public function star($id)
    {
        $client = new Client();
        $uri = "https://gitlab.com/api/v4/project/". $id."/star";
        $response = $client->post($uri);
        return redirect ('/');
    }
}

