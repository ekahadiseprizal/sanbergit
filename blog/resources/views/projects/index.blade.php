@extends('layouts.master_admin')
@section('content')
    {{-- <h1>{{$plStandings->competition->name}}</h1> --}}
{{-- <p>Last Update : {{$plStandings->competition->lastUpdated}} </p> --}}


<table class="table">
    <thead class="thead-dark"> 
        <tr>
            <th scope="col"> Project Id</th>
            <th scope="col">Name</th>
            <th scope="col">description</th>
            <th scope="col">web_url</th>
            <th scope="col">star_count</th>
            <th scope="col">owner</th>
        </tr>
        @foreach ( $projectLists as $projectList)
            <tr>
                <td> {{$projectList->id}}</td>
                <td> {{$projectList->name}}</td>
                <td> {{$projectList->description}}</td>
                <td> {{$projectList->web_url}}</td>
                <td> <i class="toggle_star far fa-star"></i> {{$projectList->star_count}}</td>
                {{-- <td> <i class="toggle_star fas fa-star"></i> {{$projectList->star_count}}</td> --}}
                <td> {{$projectList->namespace->name}}</td>

            </tr>
        @endforeach
    </table>
@endsection

@section('javascript')
    <script>
        $(document).ready(function () {

            $("#toggle_star").click(function () {
                $ajax
            });

        }
    </script>
@endsection

{{-- {
    "id": 12079224,
        "description": "",
        "name": "sanbergit",
        "name_with_namespace": "eka hadi seprizal / sanbergit",
        "path": "sanbergit",
        "path_with_namespace": "ekahadiseprizal/sanbergit",
        "created_at": "2019-04-29T07:10:41.633Z",
        "default_branch": "master",
        "tag_list": [],
        "ssh_url_to_repo": "git@gitlab.com:ekahadiseprizal/sanbergit.git",
        "http_url_to_repo": "https://gitlab.com/ekahadiseprizal/sanbergit.git",
        "web_url": "https://gitlab.com/ekahadiseprizal/sanbergit",
        "readme_url": "https://gitlab.com/ekahadiseprizal/sanbergit/blob/master/README.md",
        "avatar_url": null,
        "star_count": 0,
        "forks_count": 0,
        "last_activity_at": "2019-04-29T07:10:41.633Z",
        "namespace": {
            "id": 4990414,
            "name": "ekahadiseprizal",
            "path": "ekahadiseprizal",
            "kind": "user",
            "full_path": "ekahadiseprizal",
            "parent_id": null
        }
    } --}}